- Idea Name: (fill in with a unique name, ie `my_awesome_feature`)
- Start Date: (fill in with today's date, YYYY-MM-DD)

## Motivation
Why are we doing this? What improvements should we expect? What should the result be?

## Explanation
Explain the proposal as though it's already implemented and you're teaching it to someone else who's already familiar with Encounter.
- Explain what's new.
- Explain largely in terms of examples.
- How should we think when interacting with this idea?

For RFCs regarding internals, this section should treat other contributors as the user, and explain how this affects them and the platform.

## Implementation
This is the technical portion of the RFC. Explain the idea in sufficient detail that:
- Its interaction with other features and components is clear.
- It is reasonably clear how the idea would be implemented.
- Corner cases are dissected by example.
The section should return to the examples given in the previous section, and explain more fully how the detailed proposal makes those examples work.

## Drawbacks
Why might we want to *not* do this?

## Rationale and Alternatives
- Why is this idea the best in the space of possible ideas?
- What other ideas have been considered and what is the rationale for not choosing them?
- What is the impact of not doing this?

## In the field
- Does this idea exist elsewhere?
- How have other communities responded to this feature elsewhere?
